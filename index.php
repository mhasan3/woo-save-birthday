<?php
/**
 * Plugin Name: Woo Save birthday
 * Plugin URI: https://mhasan3.github.io/
 * Description: A plugin to save birthday on woocommerce user checkout.
 * Version: 1.0
 * Author: mahmud
 * Author URI: https://mhasan3.github.io/
 * Text Domain: wsb
 * Domain Path: /languages/
 *
 */
//
if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
}

if ( !function_exists('wsb_enqueue_admin_scripts') ) {
    function wsb_enqueue_admin_scripts(){
        wp_enqueue_script( 'wsb-admin-script', plugin_dir_url( __FILE__ ) . 'js/custom.js', array('jquery'), '', true );
        wp_localize_script( 'wsb-admin-script', 'wsb_data',
            array(
                'wsb_nonce' => wp_create_nonce( 'updates' ),
                'ajax_url' => admin_url( 'admin-ajax.php' ),
            )
        );
    }
    add_action( 'admin_enqueue_scripts', 'wsb_enqueue_admin_scripts' );
}

if ( !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
    add_action( 'admin_notices', 'wsb_check_woocommerce' );

    /**
     * Ajax install & activate WooCommerce
     *
     * @since 1.0
     * @link https://developer.wordpress.org/reference/functions/wp_ajax_install_plugin/
     */
    add_action("wp_ajax_wsb_ajax_install_plugin" , "wp_ajax_install_plugin");

    return;
}



/**
 * Called when WooCommerce is inactive to display an inactive notice.
 *
 * @since 1.0
 */
function wsb_check_woocommerce() {
    if ( current_user_can( 'activate_plugins' ) ) {
        if ( !is_plugin_active( 'woocommerce/woocommerce.php' ) && !file_exists( WP_PLUGIN_DIR . '/woocommerce/woocommerce.php' ) ) {
            ?>

            <div id="message" class="error">
                <p><?php printf( __( 'Hold on sparky, Woo Save birthday requires %1$s WooCommerce %2$s to be activated.', 'wsb' ), '<strong><a href="https://wordpress.org/plugins/woocommerce/" target="_blank">', '</a></strong>' ); ?></p>
                <p><a class="install-now button wsb-plugin-install" data-plugin="woocommerce"><?php esc_attr_e( 'Alright, Install Now!', 'wsb' ); ?></a></p>
            </div>

            <?php
        } elseif ( !is_plugin_active( 'woocommerce/woocommerce.php' ) && file_exists( WP_PLUGIN_DIR . '/woocommerce/woocommerce.php' ) ) {
            ?>

            <div id="message" class="error">
                <p><?php printf( __( 'Hold on sparky, Woo Save birthday requires %1$s WooCommerce %2$s to be activated.', 'wsb' ), '<strong><a href="https://wordpress.org/plugins/woocommerce/" target="_blank">', '</a></strong>' ); ?></p>
                <p><a href="<?php echo get_admin_url(); ?>plugins.php?_wpnonce=<?php echo wp_create_nonce( 'activate-plugin_woocommerce/woocommerce.php' ); ?>&action=activate&plugin=woocommerce/woocommerce.php" class="button activate-now button-primary"><?php esc_attr_e( 'Activate', 'wsb' ); ?></a></p>
            </div>

            <?php
        } elseif ( version_compare( get_option( 'woocommerce_db_version' ), '2.5', '<' ) ) {
            ?>

            <div id="message" class="error">
                <p><?php printf( __( '%s Woo Save birthday is inactive.%s This plugin requires WooCommerce 2.5 or newer. Please %supdate WooCommerce to version 2.5 or newer%s', 'wsb' ), '<strong>', '</strong>', '<a href="' . admin_url( 'plugins.php' ) . '">', '&nbsp;&raquo;</a>' ); ?></p>
            </div>

            <?php
        }
    }
}

class SAVE_BIRTHDAY{
    public function __construct(){
        add_action('woocommerce_checkout_fields', [$this, 'mh_woocommerce_checkout_fields']);
        register_activation_hook( __FILE__, [$this, 'table_to_store_birthdays'] );
        add_action( 'woocommerce_checkout_update_user_meta', [$this , 'mh_woocommerce_checkout_update_user_meta'], 10, 2 );
        add_action( 'woocommerce_edit_account_form', [$this , 'show_birthday_of_user'] );
        add_action( 'admin_menu', [$this, 'show_all_user_birthday'] );
    }

    function mh_woocommerce_checkout_fields( $checkout_fields = array() ) {

        $checkout_fields['order']['date_of_birth'] = array(
            'type'          => 'date',
            'class'         => array('my-field-class form-row-wide'),
            'label'         => __('Date of Birth'),
            'placeholder'   => __('dd/mm/yyyy'),
            'required'      => true,
        );

        return $checkout_fields;
    }


    function table_to_store_birthdays() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'birthday_by_email';
        $pf_parts_db_version = '1.0.0';
        $charset_collate = $wpdb->get_charset_collate();

        if ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_name}'" ) != $table_name ) {

            $sql = "CREATE TABLE $table_name (
                        id mediumint(9) NOT NULL AUTO_INCREMENT,
                        email varchar (255) NOT NULL UNIQUE ,
                        birthday datetime NOT NULL,
                        PRIMARY KEY  (id)
                        ) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
            add_option( 'pf_parts_db_version', $pf_parts_db_version );
        }

        $row = $wpdb->get_results(  "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_name = '". $table_name ."' AND column_name = 'user_name'"  );

        if(empty($row)){
            $sql = "ALTER TABLE $table_name  ADD `user_name` varchar (255)";
            $wpdb->query($sql);
        }
    }

    function mh_woocommerce_checkout_update_user_meta( $customer_id, $posted ) {
        global $wpdb;
        $tablename = $wpdb->prefix.'birthday_by_email';
        if (isset($posted['date_of_birth'])) {
            $dob = sanitize_text_field( $posted['date_of_birth'] );
            update_user_meta( $customer_id, 'date_of_birth', $dob);
            $wpdb->replace( $tablename, array(
                'birthday' => $posted['date_of_birth'],
                'email' => $posted['billing_email'],
                'user_name' => $posted['billing_first_name']),
                array( '%s', '%s', '%s' )
            );
        }
    }

    function show_birthday_of_user() {
        $user = wp_get_current_user();
        ?>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="favorite_color"><?php _e( 'Birthday', 'woocommerce' ); ?>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="favorite_color" id="favorite_color" value="<?php echo esc_attr( $user->date_of_birth ); ?>" />
        </p>
        <?php
    }

    public function show_all_user_birthday(){
        add_menu_page(
            'All User\'s Birthday',
            'User\'s Birthday',
            'manage_options',
            'myplugin/myplugin-admin-page.php',
           [$this, 'get_all_user_birthday'] , 'dashicons-tickets', 6  );
    }

    public function get_all_user_birthday(){
        global $wpdb;
        $res = $wpdb->get_results(
            "select * from wp_birthday_by_email"
        );

        if($res):
            echo '<pre>';
            print_r($res);
        endif;
    }

}

new SAVE_BIRTHDAY();