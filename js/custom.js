jQuery(document).ready(function($){
    $('.wsb-plugin-install').on('click', function (e) {
       e.preventDefault();
        var current = $(this);
        var plugin_slug = current.attr("data-plugin");

        current.addClass('updating-message').text('Installing...');

        var data = {
            action: 'wsb_ajax_install_plugin',
            _ajax_nonce: wsb_data.wsb_nonce,
            slug: plugin_slug,
        };
        jQuery.post( wsb_data.ajax_url, data, function(response) {
            //console.log(response);
            //console.log(response.data.activateUrl);
            current.removeClass('updating-message');
            current.addClass('updated-message').text('Installed!');
            current.attr("href", response.data.activateUrl);
        })
            .fail(function() {
                current.removeClass('updating-message').text('Failed!');
            })
            .always(function() {
                current.removeClass('install-now updated-message').addClass('activate-now button-primary').text('Activating...');
                current.unbind(e);
                current[0].click();
            });
    });
});